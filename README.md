# Project for ELEC5619 "HealthTogether" #

This is a Meetup website that focuses on Health related topics.

Based on Spring MVC, Hibernate and Maven, also using MySQL database.

# Set up #
Environment 

*   STS(version sts-3.7.3.RELEASE or above)

*   Tomcat v8.0

*   MySQL server 

*   java-version:1.8

*   springframework-version:3.1.1.RELEASE

*   spring.security.version:3.1.1.RELEASE

*   Maven version:4.0.0
 
Configuration(as .properties file are ignored)

* config.properties:

```
list.of.category=All,Healthy Diet,Sports,Reading,Traveling,Music
rootPath= path to store images(e.g.D:/resources/imgs/)
```
              
* database.properties:

```
jdbc.databaseName=****

jdbc.url=jdbc:mysql://localhost:3306/****?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull

jdbc.username=****

jdbc.password=****

```
* message.properties:

```
meeting.title.required=Title of Meeting is required

meeting.date.required=Date of Meeting is required 

meeting.description.required=Description of Meeting is required

error.meeting.dated_date=This date is expired

grouping.title.required=Title of Group is required

grouping.location.required=Location of Group is required

grouping.description.required=Description of Group is required

grouping.category.required=Please choose a associated category for your Group 
```

Add the above .properties file under the resources directory
,and Bootstrap js and css files are already included in our project
  
# Content #
This project repository contains three server components

*  1 Domain

*  2 Service

*  3 Web   

and also front-end component : *.jsp file

## About Domain ##

It consists of POJOs needed in our project:

* Category

* User

* UserRole

* Grouping

* Meeting

## About Service ##

It consists of logic services(add/delete/update) of each domain :

* CategoryService

* UserService

* UserRoleService

* GroupingService

* MeetingService

## About Web ##

It consists of logic services(add/delete/update) of each domain :

* CategoryController

* EditController

* UserController

* GroupingController

* MeetingController

* HomeController

* RegController