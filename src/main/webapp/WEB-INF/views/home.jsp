<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<head>
<title>HealthTogether</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-inverse" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">HealthTogether</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="<c:url value="/"/>">Home</a></li>

							<li class="dropdown"><a class="dropdown-toggle"
								data-toggle="dropdown">Category<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<c:forEach items="${categoryList}" var="cl">
										<li><a class="category" id="${cl}" href="${cl}"><c:out
													value="${cl}" /></a></li>
									</c:forEach>
								</ul></li>
							<!-- <li><a href="#">About Us</a></li> -->
						</ul>


						<form id="search_form" class="navbar-form navbar-left" action="search_group"
							method="post">
							<div class="form-group">
								<input type="text" class="form-control" name="value"
									placeholder="Search">
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
						</form>

						<ul class="nav navbar-nav navbar-right">
							<li><a href="<c:url value="category"/>">Category</a></li>
							<li class="active"><a href="<c:url value="add_group"/>">
									Add a Group </a></li>
							<li><a id="uname" style="display: none"
								href="<c:url value="user/edit" />">${user.username}</a></li>
							<c:choose>
								<c:when test="${user.id > 0}">
									<script type="text/javascript">
										$(document).ready(function() {
										document.getElementById("uname").style.display = "inline-block";
										})
									</script>
									<li><a href="<c:url value="j_spring_security_logout" />">Logout</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="<c:url value="register"/>">Sign Up</a></li>
									<li class="dropdown"><a href="#" class="dropdown-toggle"
										data-toggle="dropdown">Sign in <b class="caret"></b></a>
										<ul id="dropdown-menu" class="dropdown-menu"
											style="padding: 15px; min-width: 250px;">
											<li>
												<div class="row">
													<div class="col-md-12">
														<form name='loginForm'
															action="<c:url value='j_spring_security_check' />"
															method='POST' id="login-nav">

															<div class="form-group">
																<label class="sr-only" for="exampleInputEmail2">User
																	Name</label> <input type='text' class="form-control"
																	name="username" placeholder="User Name" required>
															</div>
															<div class="form-group">
																<label class="sr-only" for="exampleInputPassword2">Password</label>
																<input type='password' class="form-control"
																	name="password" placeholder="Password" required>
															</div>
															<div class="checkbox">
																<label> <input type="checkbox"> Remember
																	me
																</label>
															</div>
															<div class="form-group">
																<button  type="submit"
																	class="btn btn-success btn-block">Sign in</button>
															</div>
														</form>
													</div>
												</div>
											</li>
											<!-- <li class="divider"></li>
                                        <li><input class="btn btn-primary btn-block" type="button"
                                            id="sign-in-google" value="Sign In with Google"> <input
                                            class="btn btn-primary btn-block" type="button"
                                            id="sign-in-twitter" value="Sign In with Twitter">
                                        </li> -->
										</ul></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
	</div>

	<%--<jsp:include page="header.jsp"/>--%>

	<c:if test="${ resultGroup.size() > 0 }">
		<c:forEach items="${resultGroup}" var="gr" varStatus="count">
			<li><c:out value="${gr.title}" /></li>
		</c:forEach>
	</c:if>

	<div id="partial_group" style="height: 100%">

		<jsp:include page="partial_group.jsp" />

	</div>

	<%@ include file="/WEB-INF/views/footer.jsp"%>

</body>

<script type="text/javascript">
	$(document).ready(function() {
		//Handles menu drop down
		//        $('.dropdown-menu').find('form').click(function (e) {
		$('#dropdown-menu').find('form').click(function(e) {
			e.stopPropagation();
		});

		$(".category").click(function(e) {
			e.preventDefault();
			$("#partial_group").load($(this).attr("href"));
		});

		$('#search_form').submit(function() { // catch the form's submit event
		    $.ajax({ // create an AJAX call...
		        data: $(this).serialize(), // get the form data
		        type: $(this).attr('method'), // GET or POST
		        url: $(this).attr('action'), // the file to call
		        success: function(response) { // on success..
		           $("#partial_group").html(response); // update the DIV
		            //$("#partial_group").load(response);
		        }
		    });
		    return false; // cancel original event to prevent form submitting
		});

		document.getElementById("All").click();

	})
</script>
