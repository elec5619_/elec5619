<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<html>
<head>
    <title>Edit group</title>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript"
            src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyA_nSni_cbBkZe_51lTil-4Bk1y4gOgFkM"></script>
    <script src="<c:url value="/resources/js/locationpicker.jquery.js" />"></script>

</head>

<body>

<%-- 	<%@ include file="/WEB-INF/views/header.jsp"%> --%>

<div class="container">
    <h3>Edit this Group</h3>
    <div class="row">
        <div class="col-xs-8 well">
            <sf:form method="post" modelAttribute="group" enctype="multipart/form-data">
                <fieldset class="form-group">
                    <label for="recipetitle">Title</label>
                    <sf:textarea class="form-control" path="title" id="title" rows="1"
                                 maxlength="30"/>
                    <small class="text-muted">Edit the title of your Group (30
                        characters max)
                    </small>
                </fieldset>
                <div style="margin-bottom: 15px;">
                    <sf:errors path="title" class="alert alert-warning"/>
                </div>

                <fieldset class="form-group">
                    <label for="category">Select a Category:</label>
                    <sf:select class="form-control" id="category" path="category">
                        <c:forEach items="${categoryList}" var="cl">
                            <sf:option value="${cl}"/>
                        </c:forEach>
                    </sf:select>
                </fieldset>

                <div style="margin-bottom: 15px;">
                    <sf:errors path="category" class="alert alert-warning"/>
                </div>

                <fieldset class="form-group">
                    <label for="description">Description:</label>
                    <sf:textarea class="form-control" id="description"
                                 path="description" rows="3" maxlength="800"
                                 placeholder="Enter description of your Group"/>
                    <small class="text-muted">Describe about your Group (800
                        characters max)
                    </small>
                </fieldset>

                <div style="margin-bottom: 15px;">
                    <sf:errors path="description" class="alert alert-warning"/>
                </div>

                <fieldset class="form-group" style="margin-bottom: 20px;">
                    <label for="us3-address">Location:</label>
                    <sf:input type="text" class="form-control" id="us3-address"
                              path="location"/>
                    <small class="text-muted">Please select a location of your
                        group
                    </small>
                </fieldset>

                <div style="margin-bottom: 15px;">
                    <sf:errors path="location" class="alert alert-warning"/>
                </div>

                <div id="locationPicker" class="img-responsive"
                     style="width: 500px; height: 400px;"></div>

                <script>
                    $(document)
                            .ready(
                                    function () {

                                        var geocoder = new google.maps.Geocoder();
                                        var address = document
                                                .getElementById("us3-address").value;
                                        var latitude;
                                        var longitude;

                                        geocoder
                                                .geocode(
                                                        {
                                                            'address': address
                                                        },
                                                        function (results,
                                                                  status) {

                                                            if (status == google.maps.GeocoderStatus.OK) {
                                                                latitude = results[0].geometry.location
                                                                        .lat();
                                                                longitude = results[0].geometry.location
                                                                        .lng();
                                                                console
                                                                        .log(latitude
                                                                                + " "
                                                                                + longitude);
                                                            }

                                                            $(
                                                                    '#locationPicker')
                                                                    .locationpicker(
                                                                            {
                                                                                location: {
                                                                                    latitude: latitude,
                                                                                    longitude: longitude
                                                                                },
                                                                                radius: 300,
                                                                                inputBinding: {

                                                                                    locationNameInput: $('#us3-address')
                                                                                },
                                                                                enableAutocomplete: true,
                                                                                onchanged: function (currentLocation,
                                                                                                     radius,
                                                                                                     isMarkerDropped) {
                                                                                    // Uncomment line below to show alert on each Location Changed event
                                                                                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                                                                                },

                                                                            });

                                                        });

                                    })
                </script>
                <br>

                <fieldset class="form-group">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>File input(accept .jpg .png file)</label> <input
                                type="file" accept=".jpg,.png" name="file"/>
                        </div>
                    </div>

                </fieldset>


                <sf:hidden path="id"/>
                <%-- <sf:hidden path="members" /> --%>
                <sf:hidden path="imageUrl"/>
                <button type="submit" class="btn btn-primary" name="create-button">Edit
                    this Group
                </button>

            </sf:form>
            <a href="${pageContext.request.contextPath}">
                <button class="btn btn-danger" name="cancel-button"
                        value="delete-recipe-button">Cancel
                </button>
            </a>
        </div>
    </div>
</div>


<%@ include file="/WEB-INF/views/footer.jsp" %>

</body>
</html>