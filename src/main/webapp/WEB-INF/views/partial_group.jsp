<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Groups of Category</title>

    <style type="text/css">

        div#frm * {
            display: inline
        }

    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <c:forEach items="${partial_group}" var="gr" varStatus="count">
            <%--<a href="group/${gr.id}"></a>--%>
            <div class="col-sm-4">
                <div class="panel panel-success">
                    <div class="panel-heading" id="frm">
                        <c:out value="${gr.title}"/>
                    </div>
                    <a href="group/${gr.id}">
                        <div class="panel-body">
                            <img src="<c:url value="/images/${gr.imageUrl}"/>"
                                 class="img-responsive" style="width: 100%; height: 160px"
                                 alt="Image not uploaded">
                        </div>
                    </a>
                    <div class="panel-footer">
                        <c:out value="At: ${gr.location}"/>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
<br>

</body>
</html>



