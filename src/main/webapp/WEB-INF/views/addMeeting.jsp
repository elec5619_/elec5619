<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Add a Meeting</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#datepicker").datepicker();
        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/views/header.jsp" %>

<div class="container">
    <h3>Create a new Meeting</h3>
    <div class="row">
        <div class="col-xs-8 well">
            <sf:form action="add_meeting" method="post" modelAttribute="meeting">

                <div class="form-group">
                    <label>Meeting's Title:</label> <br>
                    <div>
                        <sf:textarea class="form-control" path="title" rouws="1"
                                     placeholder="Enter Meeting's Title" maxlength="30"/>
                    </div>
                    <small class="text-muted">Give the title of your Meeting
                        (30 characters max)
                    </small>

                </div>

                <div style="margin-top: 15px;">
                    <sf:errors path="title" class="alert alert-warning"/>
                </div>

                <fieldset class="form-group">
                    <label for="description">Description:</label>
                    <sf:textarea class="form-control" id="description"
                                 path="description" rows="2" maxlength="70"
                                 placeholder="Enter description of your Group"/>
                    <small class="text-muted">Describe about your Meeting (70
                        characters max)
                    </small>
                </fieldset>

                <div style="margin-top: 15px;">
                    <sf:errors path="description" class="alert alert-warning"/>
                </div>

                <div class="form-group" style="margin-top: 20px;">
                    <label>Date:</label> <br/>
                    <!-- http://jqueryui.com/datepicker/ -->
                    <sf:input type="text" rows="2" path="date" id="datepicker"
                              name="date"/>
                    <sf:errors path="date" class="alert alert-warning"/>
                </div>

                <br>
                <input type="submit" value="Add Meeting" class="btn btn-primary">

            </sf:form>

            <a href="<c:url value="/group/${groupid}"/>">
                <button class="btn btn-default">Cancel</button>
            </a>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/views/footer.jsp" %>
</body>
</html>