
<html>
<head>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- Brand and toggle get grouped for better mobile display -->

            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">HealthTogether</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse"
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<c:url value="/"/>">Home</a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<c:url value="category"/>">Category</a></li>

                <li><a id="uname" style="display: none"
                       href="<c:url value="/user/edit" />">${user.username}</a></li>
                <c:choose>
                    <c:when test="${user.id > 0}">
                        <li>${user.username}</li>
                        <li><a href="<c:url value="/j_spring_security_logout" />">Logout</a></li>

                        <script type="text/javascript">
                            $(document).ready(
                                    function () {
                                        document.getElementById("uname").style.display = "inline-block";
                                        document.getElementById("signin").style.display = "none";
                                    });
                        </script>
                    </c:when>

                    <c:otherwise>
                        <li><a href="<c:url value="/register"/>">Sign Up</a></li>

                        <li class="dropdown"><a href="#" class="dropdown-toggle"
                                                data-toggle="dropdown">Sign in <b class="caret"></b></a>
                            <ul id="dropdown-menu" class="dropdown-menu"
                                style="padding: 15px; min-width: 250px;">
                                <li>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form name='loginForm'
                                                  action="<c:url value='/j_spring_security_check' />"
                                                  method='POST' id="login-nav">

                                                <div class="form-group">
                                                    <label class="sr-only" for="exampleInputEmail2">User
                                                        Name</label> <input type='text' class="form-control"
                                                                            name="username" placeholder="User Name"
                                                                            required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                    <input type='password' class="form-control"
                                                           name="password" placeholder="Password" required>
                                                </div>
                                                <div class="checkbox">
                                                    <label> <input type="checkbox"> Remember
                                                        me
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success btn-block">Sign
                                                        in
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
</body>
</html>