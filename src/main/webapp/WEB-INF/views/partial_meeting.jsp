<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Meeting</title>
    <style type="text/css">
        .well {
            margin-left: 10px;
        }

        h4 {
            margin-left: 10px;
        }

        div#frm * {
            display: inline
        }
    </style>

</head>
<body>
<h4><span class="label label-default">${count}</span>
    Meeting List
</h4>

<c:forEach items="${partialMeeting}" var="meeting" varStatus="count">

    <div class="well">
        <h3>${meeting.title}</h3>
        <h5><span class="glyphicon glyphicon-time"></span><fmt:formatDate pattern="MM/dd/yyyy" value="${meeting.date}"/>
        </h5>

        <div id="frm">
            <c:choose>
                <c:when test="${owned == true}">
                    <a href="<c:url value="${groupid}/edit_meeting/${meeting.id}"/>"><span
                            class="label label-success">Edit</span></a>
                    <form class="inline-form" action="<c:url value="${groupid}/delete_meeting"/>" method="post">
                        <input type="hidden" name="meetingId" value="${meeting.id}"/>
                        <input type="hidden" name="groupId" value="${groupid}"/>
                        <button class="label label-danger" type="submit"
                                onClick="return confirm('Are you sure to delete this Meeting?')">Delete
                        </button>
                    </form>
                </c:when>
                <c:otherwise/>
            </c:choose>
            <form action="${groupid}/meeting/${meeting.id}/join_leave" method="post">
                <input type="hidden" name="meetingId" value="${meeting.id}"/>
                <c:choose>
                    <c:when test="${meeting.members.contains(user)}">
                        <button type="submit">Leave</button>
                    </c:when>
                    <c:otherwise>
                        <button type="submit">Join</button>
                    </c:otherwise>
                </c:choose>
            </form>
        </div>

        <p>${meeting.description}</p>
    </div>

</c:forEach>

</body>
</html>