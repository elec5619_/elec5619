<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<body>

<%@include file="header.jsp"%>
<div class="section" >
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin:0px 0px 30px 0px">
                <h1 class="text-center">User Detail</h1>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-7" style="margin:0px 0px 30px 0px">
              <a class="btn btn-info" href="<c:url value="/user/founded_groups"/>"
              role="button">Founded groups</a>
              <a class="btn btn-info" href="<c:url value="/user/joint_groups"/>"
              role="button">Joint groups</a>
              <a class="btn btn-info" href="<c:url value="/user/organized_meetings"/>"
              role="button">Organized meetings</a>
              <a class="btn btn-info" href="<c:url value="/user/joint_meetings"/>"
              role="button">Joint meetings</a>       
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png"
                     class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-13">
                <sf:form  method="post" modelAttribute="user"
                         enctype="enctype=application/x-www-form-urlencoded">
                    <div class="form-group" >
                        <div class="col-sm-1" style="margin:0px 0px 10px 0px">
                            <label for="inputEmail3" contenteditable="true" class="control-label">Email</label>
                        </div>
                        <div class="col-sm-5" style="margin:0px 0px 10px 0px">
                            <sf:input type="email" path="email" class="form-control"
                                      id="inputEmail3" placehold="email"/>
                        </div>
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputLast_login3" class="control-label">Lastlogin</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Last_Login" class="form-control" id="inputLast_login3"
                                      disabled="disabled" placeholder="Last_Login"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputPassword3" contenteditable="true" class="control-label">Password</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="password" class="form-control"
                                      id="inputPassword3" placehold="password"/>
                        </div>
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputIHometown3" class="control-label">Hometown</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Hometown" class="form-control"
                                      id="inputIHometown3" placeholder="Hometown"/>
                        </div>
                    </div>
                    <div align="left" class="form-group">
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputUsername3" class="control-label">Username</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Username" class="form-control"
                                      id="inputUsername3" placeholder="username"
                                      disabled="disabled"/>
                        </div>
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputInterest3" contenteditable="true" class="control-label">Interest</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Interest" class="form-control"
                                      id="inputInterest3" placeholder="Interest"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputPhone3" contenteditable="true" class="control-label">Phone</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Phone" class="form-control"
                                      id="inputPhone3" placeholder="Phone"/>
                        </div>
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputLocation3" contenteditable="true" class="control-label">Location</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Location" class="form-control" id="inputLocation3"
                                      placeholder="Location"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputAddress3" contenteditable="true" class="control-label">Address</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Address" class="form-control"
                                      id="inputAddress3" placeholder="Address"/>
                        </div>
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputDate_join3" class="control-label">Date join</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Date_Joined" class="form-control" id="inputDate_join3"
                                      placeholder="Date_Joined" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputBirthday3" contenteditable="true" class="control-label">Birthday</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Birthday" class="form-control"
                                      id="inputBirthday3" placeholder="Birthday"/>
                        </div>
                        <div class="col-sm-1"style="margin:0px 0px 10px 0px">
                            <label for="inputBio3" contenteditable="true" class="control-label">Bio</label>
                        </div>
                        <div class="col-sm-5"style="margin:0px 0px 10px 0px">
                            <sf:input type="text" path="Bio" class="form-control"
                                      id="inputBio3" placeholder="Bio"/>
                        </div>
                    </div>
                    <div class="button">
                        <button type="submit" style="margin:0px 0px 0px 1100px ;
    			    width:100px"
                                class="btn btn-success btn-block"> save
                        </button>
                    </div>
                </sf:form>
            </div>

        </div>
    </div>
</div>

<%--  <a style="margin:0px 0px 0px 1100px ;
       width:100px" href="<c:url value="edit" />">save</a>
 </body> --%>

</html>