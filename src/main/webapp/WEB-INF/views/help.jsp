<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>

<html>
	<head>
		<title> Help Center </title>
		
		<style>			
			.topnav {
			  list-style-type: none;
			  margin: 0;
			  padding: 0;
			  overflow: hidden;
			  background-color: black;
			}
			
			.topnav li {float: left;}
			
			.topnav li a {
			  display: inline-block;
			  color: #f2f2f2;
			  text-align: center;
			  padding: 14px 16px;
			  text-decoration: none;
			  transition: 0.3s;
			  font-size: 17px;
			}
			
			.topnav li a:hover {background-color: #555;}

			.topnav li.icon {display: none;}
			
			@media screen and (max-width:680px) {
			  ul.topnav li:not(:first-child) {display: none;}
			  ul.topnav li.icon {
			    float: right;
			    display: inline-block;
			  }
			}
			
			@media screen and (max-width:680px) {
			  ul.topnav.responsive {position: relative;}
			  ul.topnav.responsive li.icon {
			    position: absolute;
			    right: 0;
			    top: 0;
			  }
			  ul.topnav.responsive li {
			    float: none;
			    display: inline;
			  }
			  ul.topnav.responsive li a {
			    display: block;
			    text-align: left;
			  }
			}
			
			div.container {
			    width: 100%;
			    border: 1px solid gray;
			    height: 100%;
			}
			
			nav {
			    float: left;
			    width: 300px;
			    margin: 0;
			    padding: 2em;
			    border-right: 1px solid gray;
			    height: 100%;
			}
			
			nav ul {
			    list-style-type: none;
			    padding: 5px;
			}
			   
			nav ul a {
			    text-decoration: none;
			    color: black;
			}
			
			nav ul li{
				padding: 5px;
			}
			
			button.accordion {
			    background-color: #eee;
			    color: #444;
			    cursor: pointer;
			    padding: 18px;
			    width: 100%;
			    border: none;
			    text-align: left;
			    outline: none;
			    font-size: 15px;
			    transition: 0.4s;
			}
			
			button.accordion.active, button.accordion:hover {
			    background-color: #ddd;
			}
			
			button.accordion:after {
			    content: '\02795';
			    font-size: 13px;
			    color: #777;
			    float: right;
			    margin-left: 5px;
			}
			
			button.accordion.active:after {
			    content: "\2796";
			}
			
			div.panel {
			    padding: 0 18px;
			    background-color: white;
			    max-height: 0;
			    overflow: hidden;
			    transition: 0.6s ease-in-out;
			    opacity: 0;
			}
			
			div.panel.show {
			    opacity: 1;
			    max-height: 500px;
			}
			
			input[type=text]{
				    width: 130px;
				    box-sizing: border-box;
				    border: 2px solid #ccc;
				    border-radius: 4px;
				    font-size: 16px;
				    background-color: white;
				    background-image: url('/css/searchicon.png');
				    background-position: 10px 10px;
				    background-repeat: no-repeat;
				    padding: 12px 20px 12px 40px;
				    -webkit-transition: width 0.4s ease-in-out;
				    transition: width 0.4s ease-in-out;
			}
			
			input[type=text]:focus{
				width: 80%;
			}
			
			article {
			    margin-left: 320px;
			    padding: 1em;
			    overflow: hidden;
			}
		</style>

	</head>
	
	<body>
		<header>
			<ul class="topnav" id="myMenu">
				<li><a href="#">HealthTogether</a></li>
				<li><a href="#">Log out</a></li>
				<li><a href="#">Sign up</a></li>
				<li><a href="#">Sign in</a></li>
			</ul>
		</header> 

		<div class="container">
			<nav>
				<button class="accordion">Introduction</button>
				<div class="panel">
						<ul>
							<li><a href="#">Introduction of HealthTogether</a></li>
							<li><a href="#">Group in HealthTogether</a></li>
							<li><a href="#">Meeting in HealthTogether</a></li>
						</ul>
				</div>
				
				<button class="accordion">Getting Started</button>
				<div class="panel">
						<ul>
							<li><a href="#">Create a group</a></li>
							<li><a href="#">Edit group</a></li>
							<li><a href="#">Create a meeting</a></li>
							<li><a href="#">Edit meeting</a></li>
						</ul>
				</div>
			</nav>
			
			<article>
				<h1>Help Center</h1>
				<p>
					You can select solutions for questions related to your problem from the left navigation.
				</p>
				<p>
					You can also search solutions by typing in key words.
				</p>
				
				<form>
				  <input type="text" name="search" placeholder="Search..">
				</form>
				
			</article>
		</div>
		
		<%@ include file="/WEB-INF/views/footer.jsp"%>
	</body>
	
	<script type="text/javascript">
	
		function myFunction(){
			var x = document.getElementById("myMenu");
			if(x.className === "topnav"){
				x.className += "responsive";
			}else{
				x.className = "topnav";
			}
		}
		
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        this.nextElementSibling.classList.toggle("show");
		  }
		}
	</script>

</html>