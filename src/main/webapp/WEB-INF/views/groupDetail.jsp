<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>GroupDetail</title>
    <style>
        /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
        .row.content {
            height: 650px
        }

        /* Set gray background color and 100% height */
        .sidenav {
            background-color: #f1f1f1;
            height: 100%;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;

            }

            .row.content {
                height: auto;
            }
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#showUpcoming").click(function (e) {
                e.preventDefault();
                $("#partial_meeting").load($(this).attr("href"));
            });

            $("#showPast").click(function (e) {
                e.preventDefault();
                $("#partial_meeting").load($(this).attr("href"));
            });

        });

        $(document).ready(function () {

            document.getElementById("showUpcoming").click();

            $("#showPast").click(function (e) {
                $("#showUpcoming").parent('li').removeClass('active')
                $(this).parent('li').addClass('active')
            });

            $("#showUpcoming").click(function (e) {
                $("#showPast").parent('li').removeClass('active')
                $(this).parent('li').addClass('active')
            });

        });

        $(document).ready(function () {

            $('#search_form').submit(function () { // catch the form's submit event
                $.ajax({ // create an AJAX call...
                    data: $(this).serialize(), // get the form data
                    type: $(this).attr('method'), // GET or POST
                    url: $(this).attr('action'), // the file to call
                    success: function (response) { // on success..
                        $("#partial_meeting").html(response); // update the DIV
                        //$("#partial_group").load(response);
                    }
                });
                return false; // cancel original event to prevent form submitting
            });


        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/views/header.jsp" %>

<div class="container-fluid fill">
    <div class="row content fill">

        <div class="col-sm-3 sidenav fill" style="margin-top: 5px;">

            <ul class="nav nav-tabs nav-stacked"
                style="margin-top: 20px">
                <h3>Founder: ${group.founder.username}</h3>

                <sf:form action="${group.id}/join_leave" method="post">
                    <input type="hidden" name="groupId" value="${group.id}"/>
                    <c:choose>
                        <c:when test="${joined == true}">
                            <li>
                                <button class="btn btn-warning btn-block"
                                        type="submit">Leave this Group
                                </button>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <button class="btn btn-warning btn-block"
                                        type="submit">Join this Group
                                </button>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </sf:form>


                <c:choose>
                    <c:when test="${owned}">
                        <a href="<c:url value="${group.id}/edit_group"/>">
                            <button class="btn btn-default btn-block ">Edit this
                                Group
                            </button>
                        </a>

                        <form action="<c:url value="delete_group"/>" method="post">
                            <input type="hidden" name="groupId" value="${group.id}"/>
                            <button class="btn btn-default btn-block" type="submit"
                                    onClick="return confirm('Are you sure to delete this Group?')">
                                Delete this Group
                            </button>

                        </form>

                        <ul class="nav nav-pills nav-stacked" style="margin-top: 20px;">
                            <li class="active"><a
                                    href="<c:url value="${group.id}/add_meeting"/>">Add new
                                Meetings</a></li>
                        </ul>
                    </c:when>
                    <c:otherwise/>
                </c:choose>
            </ul>

            <ul class="nav nav-tabs nav-stacked" style="margin-top: 20px">
                <li id="showUpcomingtab" class="active"><a id="showUpcoming"
                                                           data-toggle="tab"
                                                           href="<c:url value="${group.id}/upcoming"/>">Upcoming
                    Meetings</a></li>
                <li><a id="showPast" data-toggle="tab"
                       href="<c:url value="${group.id}/past"/>">Past Meeting</a></li>
            </ul>



            <form id="search_form" class="navbar-form navbar-left" action="<c:url value="${group.id}/search_meeting"/>"
                  method="post">
                <div class="input-group">
                    <input type="text" class="form-control"
                           placeholder="Search Meetings" name="value"> <span
                        class="input-group-btn">
							<button class="btn btn-default" type="submit">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span>
                </div>
            </form>
			<br>

            <h4>Group's Members:<span class="label label-primary"><c:out
                            value="${member_count+1}"/></span></h4>
                            
            <ul class="nav nav-pills nav-stacked" >
                <c:forEach items="${members}" var="member" varStatus="count">
                    <li><span class="label label-primary"><c:out
                            value="${count.index+1}"/></span> Name: ${member.username}</li>
                    <br>
                </c:forEach>
            </ul>


        </div>

        <div class="col-sm-9 row" style="margin-top:5px; margin-left:10px;background-color: #f1f1f1;">
            <h2 style="margin-left: 10px;">
                <small>Group's Details</small>
            </h2>
            <div class="col-sm-7">

                <hr>
                <h2>${group.title}</h2>
                <h5>
                    <span class="glyphicon glyphicon-time"></span> Post by Founder:
                    <Strong>${group.founder.username}</Strong>
                    At Location: ${group.location}
                </h5>
                <h5>
                    <span class="label label-danger">${group.category}</span>
                </h5>
                <br>
                <p>${group.description}</p>
                <br> <br>
            </div>

            <div class="col-sm-2 text-center" style="margin-top: 20px;">
                <img src="<c:url value="/images/${group.imageUrl}"/>"
                     class="img-rounded" height="130" width="200" alt="group_image">
            </div>
        </div>

        <div class="col-sm-9 row" id="partial_meeting">
            <jsp:include page="partial_meeting.jsp"/>

        </div>
    </div>
</div>
<%@ include file="/WEB-INF/views/footer.jsp" %>
</body>
</html>