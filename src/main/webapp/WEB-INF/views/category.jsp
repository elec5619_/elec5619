<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false"%>

<html>
	<head>
		<title>Category</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<style>
			.topnav {
				  list-style-type: none;
				  margin: 0;
				  padding: 0;
				  overflow: hidden;
				  background-color: black;
				}
				
				.topnav li {float: left;}
				
				.topnav li a {
				  display: inline-block;
				  color: #f2f2f2;
				  text-align: center;
				  padding: 14px 16px;
				  text-decoration: none;
				  transition: 0.3s;
				  font-size: 17px;
				}
				
				.topnav li a:hover {background-color: #555;}
	
				.topnav li.icon {display: none;}
				
				@media screen and (max-width:680px) {
				  ul.topnav li:not(:first-child) {display: none;}
				  ul.topnav li.icon {
				    float: right;
				    display: inline-block;
				  }
				}
				
				@media screen and (max-width:680px) {
				  ul.topnav.responsive {position: relative;}
				  ul.topnav.responsive li.icon {
				    position: absolute;
				    right: 0;
				    top: 0;
				  }
				  ul.topnav.responsive li {
				    float: none;
				    display: inline;
				  }
				  ul.topnav.responsive li a {
				    display: block;
				    text-align: left;
				  }
				}
				
			div.container {
			    width: 100%;
			    border: 1px solid gray;
			    height: 100%;
			}
		</style>
	</head>
	
	<body>
			<ul class="topnav" id="myMenu">
				<li><a href="#">HealthTogether</a></li>
				<li><a href="#">Log out</a></li>
				<li><a href="#">Sign up</a></li>
				<li><a href="#">Sign in</a></li>
			</ul>
		
		<div class="container">
			<h1>Category</h1>
			
			
		</div>	
		
		
		<%@ include file="/WEB-INF/views/footer.jsp"%>
	
	</body>
	
	<script>
		function myFunction(){
			var x = document.getElementById("myMenu");
			if(x.className === "topnav"){
				x.className += "responsive";
			}else{
				x.className = "topnav";
			}
		}
	</script>

</html>