<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
    rel="stylesheet" type="text/css">
    <title>User Meeting</title>
</head>
<body>

<%@ include file="/WEB-INF/views/header.jsp" %>

<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p class="text-info">
              <strong>  User Name: ${user.username}</strong>
            </p>
          </div>
          <div class="col-md-12 text-center">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title ">${kind}</h3>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<%@ include file="partial_meeting.jsp" %>

</body>

</html>