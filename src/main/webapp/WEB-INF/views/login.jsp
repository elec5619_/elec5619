<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
<link href="<c:url value="/resources/bootstrap/css/signin.css" />"
	rel="stylesheet">
<title>Login Page</title>

<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}
</style>
</head>
<body>

	<div class="container">

		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>

		<form name='loginForm' class="form-signin"
			action="<c:url value='j_spring_security_check' />" method='POST'>

			<h2 class="form-signin-heading">Please Login</h2>

			<label for="username" class="sr-only">UserName</label> <input
				type='text' id="username" name='username' class="form-control"
				placeholder="UserName" required autofocus> <label
				for="inputPassword" class="sr-only">Password</label> <input
				type='password' name='password' id="inputPassword"
				placeholder="Password" class="form-control" required />

			<div class="checkbox">
				<label> <input type="checkbox" value="remember-me">
					Remember me
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" name="submit"
				type="submit" value="submit">Sign in</button>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" /> <a
				href="${pageContext.request.contextPath}"> </a>
			<!-- <button class="btn btn-danger " name="cancel-button"
				value="delete-recipe-button">Cancel</button> -->
		</form>

	</div>
	<!-- /container -->
</body>
</html>