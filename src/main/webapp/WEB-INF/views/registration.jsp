<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
    rel="stylesheet" type="text/css">
    <style type="text/css">  
    .errors {  
    font-size: 12px;  
    color: red;  
}  
</style> 
<script>
	function validate(){
		if ($("#Confirm_password").val() != $("#password").val())
		{
		window.alert("Password and confirm password don't match !");
		return false;
		}
	return true;
	}
</script>
  </head>
  
  <body>
    
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 contenteditable="false" class="text-center">Registration</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-primary">Create a new user</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <sf:form action = "register" method="post" modelAttribute="user" enctype="enctype=application/x-www-form-urlencoded">
              <div class="form-group">
                <div class="col-sm-2">
								<sf:label path="email">Email</sf:label>

							</div>
                <div class="col-sm-10">
                  <sf:input type="email" path="email" class="form-control" id="inputEmail3" placeholder="Email"/>
                </div>
              </div>
              <div style="margin-bottom: 15px;">
						<sf:errors  path="email" class="errors" />
			  </div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label for="inputPassword3" class="control-label">Password</label>
                </div>
                <div class="col-sm-10">
                  <sf:input name="password" type="password" path="password" class="form-control" id="password" placeholder="Password"/>
                </div>
              </div>
              <div style="margin-bottom: 15px;">
						<sf:errors path="password" class="errors" />
					</div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label class="control-label">Confirm password</label>
                </div>
                <div class="col-sm-10">
                  <sf:input name="Confirm_password" type="password" path="Confirm_password" class="form-control" id="Confirm_password" placeholder="Password"/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label class="control-label">Username</label>
                </div>
                <div class="col-sm-10">
                  <sf:input type="text" path="username" class="form-control" id="inputUsername3" placeholder="Username"/>
                </div>
              </div>
              <div style="margin-bottom: 15px;">
						<sf:errors path="username" class="errors" />
					</div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label class="control-label">Phone</label>
                </div>
                <div class="col-sm-10">
                  <sf:input type="text"  path="phone" class="form-control" id="inputPhone3" placeholder="Phone"/>
                </div>
              </div>
              <div style="margin-bottom: 15px;">
						<sf:errors path="phone" class="errors" />
					</div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label class="control-label">Address</label>
                </div>
                <div class="col-sm-10">
                  <sf:input type="text"  path="address" class="form-control" id="inputAdress3" placeholder="Address"/>
                </div>
              </div>
              <div style="margin-bottom: 15px;">
						<sf:errors path="address" class="errors" />
					</div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" onclick="validate()" class="btn btn-default">Sign up</button>
                </div>
              </div>
            </sf:form>
          </div>
        </div>
      </div>
    </div>
  </body>

</html>

