package au.usyd.elec5619.val;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import au.usyd.elec5619.domain.Meeting;

public class MeetingValidator implements Validator {

    //Indicate that which objects can be validated by this validator
    public boolean supports(Class<?> clazz) {
        // TODO Auto-generated method stub
        return Meeting.class.equals(clazz);
    }

    // Once this method returns,
    // spring framework binds the Errors object to the BindingResult object
    // that we use in our controller handler method.
    public void validate(Object obj, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "meeting.title.required");
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "meeting.description.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "meeting.date.required");

        Meeting meeting = (Meeting) obj;
        //Indicate the date of meeting is dated, which is invalid
      if (meeting.getDate() != null && meeting.getDate().compareTo(new Date())<=0) {
            //	rejectValue(String field, String errorCode, Object[] errorArgs, String defaultMessage)
            errors.rejectValue("date", "error.meeting.dated_date", null, "Dated date");
        }


    }

}
