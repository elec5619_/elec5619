package au.usyd.elec5619.val;

import javax.validation.Validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import au.usyd.elec5619.domain.Grouping;

public class GroupingValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        // TODO Auto-generated method stub
        return Grouping.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // TODO Auto-generated method stub
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "grouping.title.required","Title of Group is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "grouping.category.required","Please choose a associated category for your Group");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location", "grouping.location.required","Location of Group is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "grouping.description.required","Description of Group is required");
    }

}
