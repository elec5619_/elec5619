package au.usyd.elec5619.web;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.GroupingService;
import au.usyd.elec5619.service.UserService;
import au.usyd.elec5619.service.impl.CustomUserDetail;
import com.sun.tracing.dtrace.ModuleAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Configuration
@PropertySource("classpath:config.properties")
@Controller
@RequestMapping(value = "/group/")
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Value("${rootPath}")
    private String rootPath;

    @Value("#{'${list.of.category}'.split(',')}")
    private List<String> categoryList;

    @Resource(name = "groupService")
    private GroupingService groupingService;

    @Resource(name = "userService")
    private UserService userService;

    @Autowired
    @Qualifier("groupingValidator")
    private Validator validator;

    @InitBinder("group")
    private void InitBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @ModelAttribute("group")
    public Grouping createGrouping() {
        return new Grouping();
    }

    @SuppressWarnings("Duplicates")
    @ModelAttribute("user")
    public User createUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            CustomUserDetail myUserDetails = (CustomUserDetail) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            Long userId = myUserDetails.getUser().getId(); //Fetch the custom property in User class
            User user = userService.getById(userId);
            return user;
        }
        return new User();
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/{groupId}")
    public String showGroupDetails(@PathVariable("groupId") Long id,
                                   Model model) {
        Grouping group = groupingService.getById(id);
        group.setMeetings(null);
        Set<User> members = group.getMembers();
        int count=members.size();
        members.add(group.getFounder());
        User user = (User) ((BindingAwareModelMap) model).get("user");
        if (user.getId() > 0) {
            model.addAttribute("joined", members.contains(user));
            model.addAttribute("owned", group.getFounder().equals(user));
        } else {
            model.addAttribute("joined", false);
            model.addAttribute("owned", false);
        }
        model.addAttribute("group", group);
        model.addAttribute("member_count",count);
        model.addAttribute("members", members);
        return "groupDetail";
    }

    @RequestMapping(value = "{groupId}/edit_group", method = RequestMethod.GET)
    public String editGroup(@PathVariable("groupId") Long groupId,
                            Model model) {
        Grouping group = this.groupingService.getById(groupId);
        User user = (User) ((BindingAwareModelMap) model).get("user");
        if (!group.getFounder().equals(user)) {
            return "redirect:/group/" + groupId;
        }
        model.addAttribute("group", group);
        
        logger.info(group.getMembers()+" ");
        
        model.addAttribute("categoryList", categoryList);
        return "editGroup";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "{groupId}/edit_group", method = RequestMethod.POST)
    public String editGroup(@ModelAttribute("group") @Valid Grouping group,
                            @PathVariable("groupId") Long groupId,
                            BindingResult bindingResult,
                            @RequestParam MultipartFile file,
                            Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("categoryList", categoryList);
            
            return "editGroup";
        }
        User user = (User) ((BindingAwareModelMap) model).get("user");
        group.setFounder(user);
        Grouping temp_group = groupingService.getById(groupId);
        group.setFounder(temp_group.getFounder());
        group.setMembers(temp_group.getMembers());
        
        logger.info(group.getMembers()+" ");
        
        if (!file.isEmpty()) {
            File dir = new File(rootPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String imageUrl = dir.getAbsolutePath() + File.separator + file.getOriginalFilename();
            File serverFile = new File(imageUrl);
            logger.info(imageUrl);
            // write uploaded image to disk
            try {
                try (InputStream is = file.getInputStream();
                     BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile))) {
                    int i;
                    while ((i = is.read()) != -1) {
                        stream.write(i);
                    }
                    stream.flush();
                }
            } catch (IOException e) {
                e.getStackTrace();
                System.out.println("error : " + e.getMessage());
            }
            group.setImageUrl(file.getOriginalFilename());
        }
        groupingService.update(group);
        return "redirect:/group/" + group.getId();
    }

    @RequestMapping(value = "delete_group", method = RequestMethod.POST)
    public String deleteGroup(HttpServletRequest httpServletRequest) {
        Long groupId = Long.parseLong(httpServletRequest.getParameter("groupId"));
        this.groupingService.delete(groupId);
        return "redirect:/";
    }

    @RequestMapping(value = "/{groupId}/members", method = RequestMethod.GET)
    public String showMembers(@PathVariable("groupId") Long id, Model model) {
        model.addAttribute("members", this.groupingService.getMembers(id));
        return "";
    }

    @RequestMapping(value = "/{groupId}/join_leave", method = RequestMethod.POST)
    public String addMember(HttpServletRequest httpServletRequest,
                            Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        Long groupId = Long.parseLong(httpServletRequest.getParameter("groupId"));
        if (!(user.getId() > 0)) {
            return "redirect:/group/" + groupId;
        }
        Grouping group = this.groupingService.getById(groupId);
        Set<User> members = group.getMembers();
        if (members.contains(user)) {
            this.groupingService.removeMember(groupId, user);
            this.userService.leaveGroup(user, group);
        } else {
            this.groupingService.addMember(groupId, user);
            this.userService.joinGroup(user, group);
        }

        return "redirect:/group/" + groupId;
    }
    
    
 

}
