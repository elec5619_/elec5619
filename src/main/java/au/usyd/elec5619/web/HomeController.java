package au.usyd.elec5619.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import au.usyd.elec5619.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.log.Log;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.service.GroupingService;
import au.usyd.elec5619.service.UserService;
import au.usyd.elec5619.service.impl.CustomUserDetail;

/**
 * Handles requests for the application home page.
 */
@Configuration
@PropertySource("classpath:config.properties")
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    // Need to change resources mapping path in "servlet-context"
    // to assign the local path to store images
    @Value("${rootPath}")
    private String rootPath;

    // create a properties file named "config.properties" in path :"src/main/resources"
    // with content of:
    /*
      list.of.category=All,Healthy Diet,Sports,Reading,Traveling,Music
	  rootPath=the path you have assigned in in "servlet-context"
	  */

    @Value("#{'${list.of.category}'.split(',')}")
    private List<String> categoryList;

    @Resource(name = "groupService")
    private GroupingService groupingService;

    @Resource(name = "userService")
    private UserService userService;

    @Autowired
    @Qualifier("groupingValidator")
    private Validator validator;

    @ModelAttribute("group")
    public Grouping createGrouping() {
        return new Grouping();
    }

    @SuppressWarnings("Duplicates")
    @ModelAttribute("user")
    public User createUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            CustomUserDetail myUserDetails = (CustomUserDetail) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            Long userId = myUserDetails.getUser().getId(); //Fetch the custom property in User class
            User user = userService.getById(userId);
            return user;
        }
        return new User();
    }

    public void setGroupingService(GroupingService groupingService) {
        this.groupingService = groupingService;
    }

    @InitBinder("group")
    private void InitBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("categoryList", categoryList);
        return "home";
    }

    @RequestMapping(value = "/{category}", method = RequestMethod.GET)
    public String getGroupInCategory(@PathVariable("category") String category, Model model) {

        if (category.equals("All"))
            model.addAttribute("partial_group", this.groupingService.getAll());
        else
            model.addAttribute("partial_group", this.groupingService.getByCategory(category));

        return "partial_group";
    }

    @RequestMapping(value = "/add_group", method = RequestMethod.GET)
    public String addGroup(Model model) {
        model.addAttribute("categoryList", categoryList);
        return "addGroup";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/add_group", method = RequestMethod.POST)
    public String addGroup(@ModelAttribute("group") @Validated Grouping group,
                           BindingResult bindingResult,
                           @RequestParam MultipartFile file, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("categoryList", categoryList);

            return "addGroup";
        }

//        CustomUserDetail myUserDetails = (CustomUserDetail) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
//        User user = (User) myUserDetails.getUser();
        User user = (User) ((BindingAwareModelMap) model).get("user");
        group.setFounder(user);

        if (!file.isEmpty()) {
            File dir = new File(rootPath);

            if (!dir.exists()) {
                dir.mkdirs();
            }
            String imageUrl = dir.getAbsolutePath() + File.separator + file.getOriginalFilename();
            File serverFile = new File(imageUrl);
            logger.info(imageUrl);
            // write uploaded image to disk
            try {
                try (InputStream is = file.getInputStream();
                     BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile))) {
                    int i;
                    while ((i = is.read()) != -1) {
                        stream.write(i);
                    }
                    stream.flush();
                }
            } catch (IOException e) {
                e.getStackTrace();
                System.out.println("error : " + e.getMessage());
            }
            group.setImageUrl(file.getOriginalFilename());
        }

        this.groupingService.add(group);
        this.userService.foundGroup(user, group);
        return "redirect:/";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            Model model) {

        if (error != null) {
            model.addAttribute("error", "Invalid username and password!");
        }
        if (logout != null) {
            model.addAttribute("msg", "You've been logged out successfully.");
        }


        return "login";

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(String username, String password) {
        User user = userService.getByName(username, password);
        if (user != null) {
            System.out.println(username + " login success!");
            return "loginSuccess";
        }
        return "loginError";
    }

    @RequestMapping(value = "/search_group", method = RequestMethod.POST)
    public String searchGroup(Model model, HttpServletRequest httpServletRequest) {

        String keywork = httpServletRequest.getParameter("value");
        Set<Grouping> result = this.groupingService.searchGroup(keywork);
        model.addAttribute("partial_group", result);
        return "partial_group";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        return "registration";
    }

}

