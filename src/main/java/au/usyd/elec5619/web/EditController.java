package au.usyd.elec5619.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.GroupingService;
import au.usyd.elec5619.service.MeetingService;
import au.usyd.elec5619.service.UserService;
import au.usyd.elec5619.service.impl.CustomUserDetail;

@Controller 
public class EditController {
	

	@Autowired  
    private UserService userService;
	@ModelAttribute("user")
    public User createUser() {
		 CustomUserDetail customUserDetail = (CustomUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	     User user = (User) customUserDetail.getUser();
         return  user;
    }
	
  
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String handler(@ModelAttribute("user") @Valid User user, BindingResult result,Model model) {
       
            model.addAttribute("email", user.getEmail());  
            model.addAttribute("password", user.getPassword());

            model.addAttribute("Phone", user.getPhone()); 
            model.addAttribute("Address", user.getAddress()); 
            model.addAttribute("Birthday", user.getBirthday());
            model.addAttribute("Hometown", user.getHometown());
            model.addAttribute("Location", user.getLocation());
            model.addAttribute("Bio", user.getBio());
            userService.update(user); 
            return "home";  

    }  

}

