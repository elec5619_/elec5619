package au.usyd.elec5619.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserRole;
import au.usyd.elec5619.service.GroupingService;
import au.usyd.elec5619.service.MeetingService;
import au.usyd.elec5619.service.UserService;
import au.usyd.elec5619.service.UserRoleService;

@Controller
public class RegController {


    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "userRoleService")
    private UserRoleService userRoleService;

    @ModelAttribute("user")
    public User createUser() {
        return new User();
    }

    @ModelAttribute("userrole")
    public UserRole createUserRole() {
        return new UserRole();
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String handler(@ModelAttribute("user") @Valid User user, BindingResult result, UserRole userrole, Model model) {

        if (!result.hasErrors() && user.getPassword().equals(user.getConfirm_password())) {
            if (userService.findByUserName(user.getUsername()) == null) {
                model.addAttribute("email", user.getEmail());
                model.addAttribute("password", user.getPassword());
                model.addAttribute("username", user.getUsername());
                model.addAttribute("phone", user.getPhone());
                model.addAttribute("address", user.getAddress());
                model.addAttribute("Confirm_password", user.getConfirm_password());
                user.setEnabled(true);
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                user.setDate_Joined(date);
                model.addAttribute("Date_Joined", user.getDate_Joined());
                userService.add(user);
                userrole.setRole("ROLE_USER");
                userrole.setUser(user);
                model.addAttribute("role", userrole.getRole());
                userRoleService.addRole(userrole);
                return "login";
            } else

                return "registration";
        } else
            return "registration";
    }

}
