package au.usyd.elec5619.web;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.GroupingService;
import au.usyd.elec5619.service.MeetingService;
import au.usyd.elec5619.service.UserService;
import au.usyd.elec5619.service.impl.CustomUserDetail;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Controller
@RequestMapping(value = "/user/")
public class UserController {
    @Resource(name = "groupService")
    private GroupingService groupingService;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "meetingService")
    private MeetingService meetingService;

    @SuppressWarnings("Duplicates")
    @ModelAttribute("user")
    public User createUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            CustomUserDetail myUserDetails = (CustomUserDetail) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            Long userId = myUserDetails.getUser().getId(); //Fetch the custom property in User class
            User user = userService.getById(userId);
            return user;
        }
        return new User();
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String edit(Model model) {
        return "editInformation";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String handler(@ModelAttribute("user") @Valid User user,
                          BindingResult result,
                          Model model) {
        model.addAttribute("email", user.getEmail());
        model.addAttribute("password", user.getPassword());
        model.addAttribute("Phone", user.getPhone());
        model.addAttribute("Address", user.getAddress());
        model.addAttribute("Birthday", user.getBirthday());
        model.addAttribute("Hometown", user.getHometown());
        model.addAttribute("Location", user.getLocation());
        model.addAttribute("Bio", user.getBio());
        userService.update(user);
        return "redirect:/";
    }

    @RequestMapping(value = "organized_meetings", method = RequestMethod.GET)
    public String showOrganizedMeetings(Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        model.addAttribute("partialMeeting", user.getFoundMeetingList());
        model.addAttribute("kind", "organized_meetings");
        return "userMeeting";
    }

    @RequestMapping(value = "joint_meetings", method = RequestMethod.GET)
    public String showJointMeetings(Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        model.addAttribute("partialMeeting", user.getMeetingList());
        model.addAttribute("kind", "joint_meetings");
        return "userMeeting";
    }

    @RequestMapping(value = "founded_groups", method = RequestMethod.GET)
    public String showFoundedGroups(Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        model.addAttribute("partial_group", user.getFoundGroupList());
        model.addAttribute("kind", "founded_groups");
        return "userGroup";
    }

    @RequestMapping(value = "joint_groups", method = RequestMethod.GET)
    public String showJointGroups(Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        model.addAttribute("partial_group", user.getJoinGroupList());
        model.addAttribute("kind", "joint_groups");
        return "userGroup";
    }

    @RequestMapping(value = "upcoming_meetings", method = RequestMethod.GET)
    public String showUpcomingMeetings(Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        Set<Meeting> meetings = user.getMeetingList();
        List<Meeting> result = new ArrayList<Meeting>();
        int count = 0;
        for (Meeting m : meetings) {
            if (m.getDate().compareTo(new Date()) >= 0) {
                result.add(m);
                count++;
            }
        }

        model.addAttribute("count", count);
        model.addAttribute("partialMeeting", result);
        model.addAttribute("kind", "upcoming_meeting");

        return "userMeeting";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "joint_groups/leave", method = RequestMethod.POST)
    public String leaveGroup(HttpServletRequest httpServletRequest, Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        Long groupId = Long.parseLong(httpServletRequest.getParameter("groupId"));
        Grouping group = this.groupingService.getById(groupId);
        Set<User> members = group.getMembers();
        if (members.contains(user)) {
            this.groupingService.removeMember(groupId, user);
            this.userService.leaveGroup(user, group);
        }

        return "redirect:/user/joint_groups";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "joint_meetings/leave", method = RequestMethod.POST)
    public String leaveMeeting(HttpServletRequest httpServletRequest, Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        Long meetingId = Long.parseLong(httpServletRequest.getParameter("meetingId"));
        Meeting meeting = this.meetingService.getById(meetingId);
        Set<User> members = meeting.getMembers();
        if (members.contains(user)) {
            this.meetingService.removeMember(meetingId, user);
            this.userService.leaveMeeting(user, meeting);
        }

        return "redirect:/user/joint_meetings";
    }
}
