package au.usyd.elec5619.web;

import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Category;
import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.CategoryService;

@Controller
@RequestMapping(value="/category/**")
public class CategoryController{

	@Resource(name="categoryService")
	private CategoryService categoryService;



}