package au.usyd.elec5619.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.UserService;
import au.usyd.elec5619.service.impl.CustomUserDetail;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.service.GroupingService;
import au.usyd.elec5619.service.MeetingService;

@Controller
@RequestMapping(value = "/group/{groupId}/")
public class MeetingController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "meetingService")
    private MeetingService meetingService;

    @Resource(name = "groupService")
    private GroupingService groupingService;

    //Inject it in the controller class
    @Autowired
    @Qualifier("meetingValidator")
    private Validator validator;

    // ModelAttribute value should be same as used in the addGroup.jsp
    @ModelAttribute("meeting")
    public Meeting createMeeting() {
        return new Meeting();
    }

    @SuppressWarnings("Duplicates")
    @ModelAttribute("user")
    public User createUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            CustomUserDetail myUserDetails = (CustomUserDetail) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            Long userId = myUserDetails.getUser().getId(); //Fetch the custom property in User class
            User user = userService.getById(userId);
            return user;
        }
        return new User();
    }

    //The @InitBinder annotated methods will get called on each HTTP request 
    //if we don't specify the 'value' element of this annotation.
    @InitBinder("meeting")
    private void InitBinder(WebDataBinder binder) {
        binder.setValidator(validator);
        //used to format date of meeting while binding meeting
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "meeting/{meetingId}")
    public String showMeetingDetail(@PathVariable("groupId") Long groupId,
                                    @PathVariable("meetingId") Long meetingId,
                                    Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        Meeting meeting = meetingService.getById(meetingId);
        Set<User> members = meeting.getMembers();
        model.addAttribute("joined", members.contains(user));
        Grouping group = groupingService.getById(groupId);
        model.addAttribute("meeting", meeting);
        model.addAttribute("group", group);
        return "";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "upcoming")
    public String showUpcoming(@PathVariable("groupId") Long groupId, Model model) {
        Grouping group = groupingService.getById(groupId);
        User user = (User) ((BindingAwareModelMap) model).get("user");
        if (user.getId() > 0) {
            model.addAttribute("owned", group.getFounder().equals(user));
        } else {
           // model.addAttribute("joined", false);
            model.addAttribute("owned", false);
        }
        Set<Meeting> meeting_List = group.getMeetings();
        //logger.info(meeting_List+" ");
        List<Meeting> result = new ArrayList<Meeting>();
        int count = 0;
        for (Meeting m : meeting_List) {
            if (m.getDate().compareTo(new Date()) >= 0) {
                result.add(m);
                count++;
            }
        }

        model.addAttribute("partialMeeting", result);
        model.addAttribute("groupid", groupId);
        model.addAttribute("count", count);
        return "partial_meeting";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "past")
    public String showPast(@PathVariable("groupId") Long id, Model model) {
        Grouping group = groupingService.getById(id);
        User user = (User) ((BindingAwareModelMap) model).get("user");
        if (user.getId() > 0) {
            model.addAttribute("owned", group.getFounder().equals(user));
        } else {
           // model.addAttribute("joined", false);
            model.addAttribute("owned", false);
        }
        Set<Meeting> meeting_List = group.getMeetings();
        List<Meeting> result = new ArrayList<Meeting>();
        int count = 0;
        for (Meeting m : meeting_List) {
            if (m.getDate().compareTo(new Date()) < 0) {
                result.add(m);
                count++;
            }
        }

        model.addAttribute("partialMeeting", result);
        model.addAttribute("count", count);
        model.addAttribute("groupid", id);
        return "partial_meeting";
    }

    @RequestMapping(value = "add_meeting", method = RequestMethod.GET)
    public String addMeeting(@PathVariable("groupId") Long groupId,
                             Model model) {
        Grouping group = this.groupingService.getById(groupId);
        User user = (User) ((BindingAwareModelMap) model).get("user");
        if (!group.getFounder().equals(user)) {
            return "redirect:/group/" + groupId;
        }
        model.addAttribute("groupid", groupId);
        return "addMeeting";
    }


    @RequestMapping(value = "add_meeting", method = RequestMethod.POST)
    public String addMeeting(@PathVariable("groupId") Long groupId,
                             @ModelAttribute("meeting") @Validated Meeting meeting,
                             BindingResult bindingResult,
                             Model model) {
        Grouping group = this.groupingService.getById(groupId);
        User user = (User) ((BindingAwareModelMap) model).get("user");
        if (!group.getFounder().equals(user)) {
            return "redirect:/group/" + groupId;
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("groupid", groupId);
            return "addMeeting";
        }
        meeting.setGroup(group);
        meeting.setOrganizer(user);
        this.meetingService.add(meeting);
        this.userService.organizeMeeting(user, meeting);

        return "redirect:/group/" + groupId;
    }

    @RequestMapping(value = "edit_meeting/{meetingId}", method = RequestMethod.GET)
    public String editMeeting(@PathVariable("meetingId") Long meetingId,
                              @PathVariable("groupId") Long groupId,
                              Model model) {
        Meeting meeting = (Meeting) this.meetingService.getById(meetingId);
        model.addAttribute("meeting", meeting);
        model.addAttribute("groupId", groupId);

        return "editMeeting";
    }

    @RequestMapping(value = "edit_meeting/{meetingId}", method = RequestMethod.POST)
    public String editMeeting(@ModelAttribute("meeting") @Validated Meeting meeting,
                              @PathVariable("meetingId") Long meetingId,
                              HttpServletRequest httpServletRequest,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "editMeeting";
        }
        Long groupId = Long.parseLong(httpServletRequest.getParameter("groupId"));
        Meeting temp_meeting = this.meetingService.getById(meetingId);
        Grouping grouping = groupingService.getById(groupId);
        meeting.setGroup(grouping);
        meeting.setMembers(temp_meeting.getMembers());
        meeting.setOrganizer(temp_meeting.getOrganizer());
        this.meetingService.update(meeting);
        return "redirect:/group/" + groupId;
    }

    @RequestMapping(value = "delete_meeting", method = RequestMethod.POST)
    public String deleteMeeting(HttpServletRequest httpServletRequest) {
        Long groupId = Long.parseLong(httpServletRequest.getParameter("groupId"));
        Long meetingId = Long.parseLong(httpServletRequest.getParameter("meetingId"));
        this.meetingService.delete(meetingId);
        return "redirect:/group/" + groupId;
    }

    @RequestMapping(value = "meeting/{meetingId}/member")
    public String showMembers(@PathVariable("meetingId") Long meetingId,
                              Model model) {
        Set<User> members = meetingService.getById(meetingId).getMembers();
        model.addAttribute("members", members);
        return "";
    }

    @RequestMapping(value = "meeting/{meetingId}/join_leave", method = RequestMethod.POST)
    public String joinMeeting(@PathVariable("groupId") Long groupId,
                              HttpServletRequest httpServletRequest,
                              Model model) {
        User user = (User) ((BindingAwareModelMap) model).get("user");
        Long meetingId = Long.parseLong(httpServletRequest.getParameter("meetingId"));
        Meeting meeting = this.meetingService.getById(meetingId);
        Grouping group = this.groupingService.getById(groupId);
        if (!group.getMembers().contains(user)) {
            this.groupingService.addMember(groupId, user);
            this.userService.joinGroup(user, group);
        }

        Set<User> members = meeting.getMembers();
        if (members.contains(user)) {
            this.meetingService.removeMember(meetingId, user);
            this.userService.leaveMeeting(user, meeting);
        } else {
            this.meetingService.addMember(meetingId, user);
            this.userService.joinMeeting(user, meeting);
        }
        return "redirect:/group/" + groupId;
    }

    @RequestMapping(value = "search_meeting", method = RequestMethod.POST)
    public String searchMeeting(HttpServletRequest httpServletRequest,Model model,@PathVariable("groupId") Long id) {
    	String keywork=httpServletRequest.getParameter("value");
    	Set<Meeting> result=this.meetingService.searchMeetingWithinGroup(keywork,id);
    	int count=result.size();
    	model.addAttribute("partialMeeting", result);
        model.addAttribute("count", count);
        model.addAttribute("groupid", id);
        return "partial_meeting";
    }
}
