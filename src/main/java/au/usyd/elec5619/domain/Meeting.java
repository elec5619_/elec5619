package au.usyd.elec5619.domain;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static java.lang.Math.toIntExact;

@Entity
@Table(name = "Meeting")
public class Meeting implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "Id")
    private long id;

    @Column(name = "Title")
    @NotBlank
    private String title;
    
    @Column(name="Description")
    @NotBlank
    private String description;

    @Column(name = "Date")
    private Date date;

    @ManyToOne(fetch = FetchType.EAGER)
    private Grouping group;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private User organizer;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<User> members = new HashSet<User>();

    public boolean equals(Object object) {
        if (!(object instanceof Meeting))
            return false;
        if (object == this)
            return true;
        return this.id == ((Meeting) object).id;
    }

    public int hashCode() {
        return toIntExact(id);
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Grouping getGroup() {
        return group;
    }

    public void setGroup(Grouping group) {
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }


}
