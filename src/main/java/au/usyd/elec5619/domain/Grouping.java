package au.usyd.elec5619.domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import static java.lang.Math.toIntExact;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

import static java.lang.Math.toIntExact;


@Entity
@Table(name = "Grouping")
public class Grouping implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "Id")
    private long id;

    @Column(name = "Title")
    @NotBlank
    private String title;

    @Column(name = "Location")
    @NotBlank
    private String location;
    
    @Column(name="ImageUrl")
    private String imageUrl = "default.png";
    
    @Column(name="Category")
    private String category;

    @Column(name = "Description")
    @NotBlank
    private String description;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private User founder;


    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinTable(name="group_user", 
	joinColumns={@JoinColumn(name="group_ID")}, 
	inverseJoinColumns={@JoinColumn(name="member_ID")})
    private Set<User> members = new HashSet<User>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "group", cascade = CascadeType.MERGE)
    @OrderBy("date ASC")
    private Set<Meeting> meetings = new HashSet<Meeting>();

    public boolean equals(Object object) {
        if (!(object instanceof Grouping))
            return false;
        if (object == this)
            return true;
        return this.id == ((Grouping) object).id;
    }

    public int hashCode() {
        return toIntExact(id);
    }
 
    
    public Set<Meeting> getMeetings() {
		return meetings;
	}

	public void setMeetings(Set<Meeting> meetings) {
		this.meetings = meetings;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getFounder() {
        return founder;
    }

    public void setFounder(User founder) {
        this.founder = founder;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }
}
