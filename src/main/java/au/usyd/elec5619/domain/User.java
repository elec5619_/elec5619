package au.usyd.elec5619.domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.toIntExact;

/**
 * Created by Mike on 14/09/2016.
 */
@Entity
@Table(name = "User", uniqueConstraints = @UniqueConstraint(
        columnNames = {"username"}))
public class User implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "Id")
    private long id;


    @Column(name = "username", unique = true, nullable = false, length = 45)
    @NotBlank(message = "Cannot be empty!")
    private String username;

    @Column(name = "password", nullable = false, length = 60)
    @NotBlank(message = "Cannot be empty!")
    private String password;

    @Column(name = "Gender")
    private String Gender;

    @Column(name = "Phone")
    private String Phone;

    @Column(name = "Confirm_password")
    private String Confirm_password;

    @Column(name = "Birthday")
    private Date Birthday;

    @Column(name = "Email", nullable = false, length = 60)
    @NotBlank(message = "Cannot be empty!")
    private String Email;

    @Column(name = "Hometown")
    private String Hometown;

    @Column(name = "Location")
    private String location;

    @Column(name = "Photo")
    private String Photo;

    @Column(name = "Address")
    private String Address;

    @Column(name = "Bio")
    private String Bio;

    @Column(name = "Interest")
    private String Interest;
    
    @Column(name = "Last_Login")
    private Date Last_Login;

    @Column(name = "Is_Superuser")
    private Boolean Is_Superuser;

    @Column(name = "Date_Joined")
    private Date Date_Joined;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserRole> userRole = new HashSet<UserRole>();

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "founder", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Grouping> foundGroupList = new HashSet<Grouping>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "organizer", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Meeting> foundMeetingList = new HashSet<Meeting>();


    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "members", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Grouping> joinGroupList = new HashSet<Grouping>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "members", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Meeting> meetingList = new HashSet<Meeting>();

    public boolean equals(Object object) {
        if (!(object instanceof User))
            return false;
        if (object == this)
            return true;
        return this.id == ((User) object).id;
    }

    public int hashCode() {
        return toIntExact(id);
    }

    public User() {

    }

    public User(long id) {
        this.id = id;
    }

    public Set<Grouping> getJoinGroupList() {
        return joinGroupList;
    }

    public void setJoinGroupList(Set<Grouping> joinGroupList) {
        this.joinGroupList = joinGroupList;
    }

    public Set<Meeting> getMeetingList() {
        return meetingList;
    }

    public void setMeetingList(Set<Meeting> meetingList) {
        this.meetingList = meetingList;
    }

    public Set<Meeting> getFoundMeetingList() {
        return foundMeetingList;
    }

    public void setFoundMeetingList(Set<Meeting> foundMeetingList) {
        this.foundMeetingList = foundMeetingList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm_password() {
        return Confirm_password;
    }

    public void setConfirm_password(String Confirm_password) {
        this.Confirm_password = Confirm_password;
    }

    public Set<Grouping> getFoundGroupList() {
        return foundGroupList;
    }

    public void setFoundGroupList(Set<Grouping> foundGroupList) {
        this.foundGroupList = foundGroupList;
    }

    public Set<UserRole> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        this.Address = address;
    }
    public String getInterest() {
        return Interest;
    }

    public void setInterest(String Interest) {
        this.Interest = Interest;
    }

    public Date getBirthday() {
        return Birthday;
    }

    public void setBirthday(Date Birthday) {
        this.Birthday = Birthday;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getHometown() {
        return Hometown;
    }

    public void setHometown(String Hometown) {
        this.Hometown = Hometown;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    public String getBio() {
        return Bio;
    }

    public void setBio(String Bio) {
        this.Bio = Bio;
    }

    public Date getLast_Login() {
        return Last_Login;
    }

    public void setLast_Login(Date Last_Login) {
        this.Last_Login = Last_Login;
    }

    public Boolean getIs_Superuser() {
        return Is_Superuser;
    }

    public void setIs_Superuser(Boolean Is_Superuser) {
        this.Is_Superuser = Is_Superuser;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Date getDate_Joined() {
        return Date_Joined;
    }

    public void setDate_Joined(Date Date_Joined) {
        this.Date_Joined = Date_Joined;
    }
}
