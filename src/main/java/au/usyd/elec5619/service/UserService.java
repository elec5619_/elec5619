package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
public interface UserService {

	public User findByUserName(String username);
	
    public List<User> getAll();
    
    public void add(User object);

    public User getById(long id);
    
    public User getByName(String userName,String password);

    public void update(User user);

    public void delete(long id);

    public void joinGroup(User user, Grouping group);

    public void leaveGroup(User user, Grouping group);

    public void joinMeeting(User user, Meeting meeting);

    public void leaveMeeting(User user, Meeting meeting);

    public void foundGroup(User user, Grouping group);

    public void organizeMeeting(User user, Meeting meeting);
}
