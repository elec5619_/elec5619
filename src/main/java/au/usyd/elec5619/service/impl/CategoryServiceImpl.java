package au.usyd.elec5619.service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Category;
import au.usyd.elec5619.service.CategoryService;

@Service(value = "categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService{

	private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public List<Category> getCategory(){
		return this.sessionFactory.getCurrentSession().createQuery("FROM Category").list();
	}

	@Override
    public void add(Category category){
		this.sessionFactory.getCurrentSession().save(category);
    }

	@Override
    public void delete(long id){
        Session currentSession = this.sessionFactory.getCurrentSession();
        Category category = (Category) currentSession.get(Category.class, id);
        currentSession.delete(category);
    }

	@Override
    public Category getById(long id){
        Session currentSession = this.sessionFactory.getCurrentSession();
        Category category = (Category) currentSession.get(Category.class, id);
        return category;
    }

	@Override
    public void update(Category category){
        Session currentSession = this.sessionFactory.getCurrentSession();
        currentSession.update(category);
    }
}