package au.usyd.elec5619.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.usyd.elec5619.domain.UserRole;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.service.UserRoleService;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service(value = "userRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private SessionFactory sessionFactory;
 


    @Override
    public void addRole(UserRole userrole) {
        this.sessionFactory.getCurrentSession().save(userrole);
    }
    
    @Override
    public UserRole getById(long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        return (UserRole) currentSession.get(UserRole.class, id);
    }
    
    
    
    


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
    
    


}
