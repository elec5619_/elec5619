package au.usyd.elec5619.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.GroupingService;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Grouping;

@Service(value = "groupService")
@Transactional
public class GroupingServiceImpl implements GroupingService {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

    @Override
    public Set<Grouping> getAll() {
    	Set<Grouping> resultGroup = new HashSet<Grouping>(this.sessionFactory.getCurrentSession().createQuery("FROM Grouping").list());
        return resultGroup;
    }

    @Override
    public void add(Grouping group) {
        this.sessionFactory.getCurrentSession().save(group);
    }

    @Override
    public Grouping getById(long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        Grouping group = (Grouping) currentSession.get(Grouping.class, id);
        return group;
    }

    @Override
    public void update(Grouping group) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        currentSession.update(group);
    }

    @Override
    public void delete(long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        Grouping group = (Grouping) currentSession.get(Grouping.class, id);
        currentSession.delete(group);
    }

    @Override
    public Set<Meeting> getMeetings(long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        Grouping group = (Grouping) currentSession.get(Grouping.class, id);
        return group.getMeetings();
    }

    
    @Override
	public Set<Grouping> getByCategory(String Category) {
		Session currentSession=this.sessionFactory.getCurrentSession();
	    Criteria criteria = currentSession.createCriteria(Grouping.class);
        criteria.add(Restrictions.like("category", Category));
        Set<Grouping> resultGroup = new HashSet<Grouping>(criteria.list());
        return resultGroup;

	}

	@Override
    public Set<Grouping> searchGroup(String temp) {
        long id;
        try {
            id = Long.parseLong(temp);
        } catch (NumberFormatException e) {
            id = 0;
        }
        Session currentSession = this.sessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Grouping.class);
        criteria.add(Restrictions.or(
                Restrictions.eq("id", id), Restrictions.like("title", temp, MatchMode.ANYWHERE)));
        Set<Grouping> resultGroup = new HashSet<Grouping>(criteria.list());
        return resultGroup;
    }

    @Override
    public Set<User> getMembers(Long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        Grouping group = (Grouping) currentSession.get(Grouping.class, id);
        return group.getMembers();
    }

    @Override
    public void addMember(Long id, User user) {
        Session session = this.sessionFactory.getCurrentSession();
        Grouping group = (Grouping) session.get(Grouping.class, id);
        Set<User> members = group.getMembers();
        members.add(user);
        group.setMembers(members);
        session.update(group);
    }

    @Override
    public void removeMember(Long id, User user) {
        Session session = this.sessionFactory.getCurrentSession();
        Grouping group = (Grouping) session.get(Grouping.class, id);
        Set<User> members = group.getMembers();
        members.remove(user);
        group.setMembers(members);
        session.update(group);
    }
}
