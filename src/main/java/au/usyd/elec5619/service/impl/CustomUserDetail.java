package au.usyd.elec5619.service.impl;

import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import au.usyd.elec5619.domain.User;

public class CustomUserDetail implements UserDetails{

    private static final long serialVersionUID = 1L;
    private User user;

    List<GrantedAuthority> authorities=null;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities)
    {
        this.authorities=authorities;
    }

    public String getPassword() {
        return user.getPassword();
    }

    public String getUsername() {
        return user.getUsername();
    }

    public boolean isAccountNonExpired() {
    	
    	return true;
        //return user.isAccountNonExpired();
    }

    public boolean isAccountNonLocked() {
    	return true;
       // return user.isAccountNonLocked();
    }

    public boolean isCredentialsNonExpired() {
    	return true;
        //return user.isCredentialsNonExpired();
    }

    public boolean isEnabled() {

    	return user.isEnabled();
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CustomUserDetail [user=");
		builder.append(user);
		builder.append("]");
		return builder.toString();
	}

    
}