package au.usyd.elec5619.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.UserRole;
import au.usyd.elec5619.service.UserService;


import javax.annotation.Resource;


@Service(value="userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	//get user from the database, via Hibernate
	@Resource(name = "userService")
	private UserService userService;

	 private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Transactional(readOnly=true)
	@Override
	public UserDetails loadUserByUsername(final String username)
		throws UsernameNotFoundException {

		au.usyd.elec5619.domain.User user = userService.findByUserName(username);
		//logger.info(user.getUsername());
		List<GrantedAuthority> authorities =
                                      buildUserAuthority(user.getUserRole());
		 //logger.debug("role of the user" + authorities);

		CustomUserDetail customUserDetail=new CustomUserDetail();
        customUserDetail.setUser(user);
        customUserDetail.setAuthorities(authorities);
        
		return customUserDetail;
		
		
		//return buildUserForAuthentication(user, authorities);

	}

	/*// Converts au.usyd.elec5619.domain.User user to
	// org.springframework.security.core.userdetails.User
	private User buildUserForAuthentication(au.usyd.elec5619.domain.User user,
		List<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(),
			user.isEnabled(), true, true, true, authorities);
	}
*/
	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}
