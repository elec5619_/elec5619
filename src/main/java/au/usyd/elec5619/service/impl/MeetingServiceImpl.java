package au.usyd.elec5619.service.impl;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.MeetingService;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.acl.Group;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Mike on 14/09/2016.
 */

@Service(value = "meetingService")
@Transactional
public class MeetingServiceImpl implements MeetingService {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Meeting> getAll() {
        return this.sessionFactory.getCurrentSession().createQuery("SELECT * FROM Meeting").list();
    }

    @Override
    public void add(Meeting object) {
        this.sessionFactory.getCurrentSession().save(object);
    }

    @Override
    public Meeting getById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        return (Meeting) session.get(Meeting.class, id);
    }

    @Override
    public void update(Meeting object) {
        this.sessionFactory.getCurrentSession().update(object);
    }

    @Override
    public void delete(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Meeting meeting = (Meeting) session.get(Meeting.class, id);
        session.delete(meeting);
    }

    @Override
    public Grouping getGrouping() {
        return null;
    }

    @Override
    public Set<User> getMembers(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Meeting meeting = (Meeting) session.get(Meeting.class, id);
        return meeting.getMembers();
    }

    @Override
    public void addMember(Long id, User user) {
        Session session = this.sessionFactory.getCurrentSession();
        Meeting meeting = (Meeting) session.get(Meeting.class, id);
        Set<User> members = meeting.getMembers();
        members.add(user);
        meeting.setMembers(members);
        session.update(meeting);
    }

    @Override
    public void removeMember(Long id, User user) {
        Session session = this.sessionFactory.getCurrentSession();
        Meeting meeting = (Meeting) session.get(Meeting.class, id);
        Set<User> members = meeting.getMembers();
        members.remove(user);
        meeting.setMembers(members);
        session.update(meeting);
    }

	@Override
	public Set<Meeting> searchMeetingWithinGroup(String temp,Long groupId) {
	    Session currentSession = this.sessionFactory.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Meeting.class);
       Grouping group = (Grouping)currentSession.get(Grouping.class, groupId);
        criteria.add(Restrictions.and(
                Restrictions.eq("group", group),Restrictions.like("title", temp, MatchMode.ANYWHERE)));
        Set<Meeting> resultGroup = new HashSet<Meeting>(criteria.list());
        return resultGroup;
    }
	
    
}
