package au.usyd.elec5619.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.service.UserService;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private SessionFactory sessionFactory;
 
	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession()
			.createQuery("from User where username=?")
			.setParameter(0, username)
			.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}
	

	@SuppressWarnings("unchecked")
	@Override
    public List<User> getAll() {
        return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();
    }

    @Override
    public void add(User user) {
        this.sessionFactory.getCurrentSession().save(user);
    }
    
    @Override
    public User getById(long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        return (User) currentSession.get(User.class, id);
    }
    
    @Override
    public User getByName(String userName,String password) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        User user = (User) currentSession.get(User.class, userName);
        return user;
    }
    
    @Override
    public void update(User user) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        currentSession.update(user);
    }
    
    @Override
    public void delete(long id) {
        Session currentSession = this.sessionFactory.getCurrentSession();
        User user = (User) currentSession.get(User.class, id);
        currentSession.delete(user);
    }

    @Override
    public void joinGroup(User user, Grouping group) {
        Session session = this.getSessionFactory().getCurrentSession();
        Set<Grouping> groups = user.getJoinGroupList();
        groups.add(group);
        user.setJoinGroupList(groups);
        session.update(user);
    }

    @Override
    public void leaveGroup(User user, Grouping group) {
        Session session = this.getSessionFactory().getCurrentSession();
        Set<Grouping> groups = user.getJoinGroupList();
        groups.remove(group);
        user.setJoinGroupList(groups);
        session.update(user);
    }

    @Override
    public void joinMeeting(User user, Meeting meeting) {
        Session session = this.getSessionFactory().getCurrentSession();
        Set<Meeting> meetings = user.getMeetingList();
        meetings.add(meeting);
        user.setMeetingList(meetings);
        session.update(user);
    }

    @Override
    public void leaveMeeting(User user, Meeting meeting) {
        Session session = this.getSessionFactory().getCurrentSession();
        Set<Meeting> meetings = user.getMeetingList();
        meetings.remove(meeting);
        user.setMeetingList(meetings);
        session.update(user);
    }

    @Override
    public void foundGroup(User user, Grouping group) {
        Session session = this.getSessionFactory().getCurrentSession();
        Set<Grouping> groups = user.getFoundGroupList();
        groups.add(group);
        user.setFoundGroupList(groups);
        session.update(user);
    }

    @Override
    public void organizeMeeting(User user, Meeting meeting) {
        Session session = this.getSessionFactory().getCurrentSession();
        Set<Meeting> meetings = user.getFoundMeetingList();
        meetings.add(meeting);
        user.setFoundMeetingList(meetings);
        session.update(user);
    }

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
