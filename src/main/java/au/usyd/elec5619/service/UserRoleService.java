package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.UserRole;
public interface UserRoleService {

	
    public void addRole(UserRole object);

    public UserRole getById(long id);
    


}
