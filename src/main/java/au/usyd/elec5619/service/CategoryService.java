package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.domain.Category;

public interface CategoryService extends Serializable{
	
	public List<Category> getCategory();
	
    public void add(Category category);
    
    public void delete(long id);

    public Category getById(long id);

    public void update(Category category);
    
}