package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;


public interface GroupingService extends Serializable {

    public Set<Grouping> getAll();
    
    public Set<Grouping> getByCategory(String Category);

    public void add(Grouping object);

    public Grouping getById(long id);

    public void update(Grouping grouping);

    public void delete(long id);

    public Set<Meeting> getMeetings(long id);

    public Set<Grouping> searchGroup(String temp);

    public Set<User> getMembers(Long id);

    public void addMember(Long id, User user);

    public void removeMember(Long id, User user);
}
