package au.usyd.elec5619.service;

import au.usyd.elec5619.domain.Grouping;
import au.usyd.elec5619.domain.Meeting;
import au.usyd.elec5619.domain.User;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Mike on 17/09/2016.
 */
public interface MeetingService extends Serializable {

    public List<Meeting> getAll();

    public void add(Meeting object);

    public Meeting getById(long id);

    public void update(Meeting object);

    public void delete(long id);

    public Grouping getGrouping();

    public Set<User> getMembers(Long id);

    public void addMember(Long id, User user);

    public void removeMember(Long id, User user);
    
    public Set<Meeting>  searchMeetingWithinGroup(String temp,Long groupId);
}
