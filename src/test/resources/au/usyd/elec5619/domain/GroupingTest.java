package au.usyd.elec5619.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;


public class GroupingTest extends TestCase{
	
	private Grouping grouping;
	
	protected void setUp() throws Exception{
		grouping = new Grouping();
		System.out.println("test sets up!");
	}
	
	public void testSetAndGetId(){
		long testId = (long) 123;
		assertNotNull(grouping.getId());
		grouping.setId(testId);
		assertEquals(testId, grouping.getId());
		System.out.println("test Id.");
	}
	
	public void testSetAndGetTitle(){
		String testTitle = "aTitle";
		assertNull(grouping.getTitle());
		grouping.setTitle(testTitle);
		assertEquals(testTitle, grouping.getTitle());
		System.out.println("test Title.");
	}
	
	public void testSetAndGetLocation(){
		String testLocation = "aLocation";
		assertNull(grouping.getLocation());
		grouping.setLocation(testLocation);
		assertEquals(testLocation, grouping.getLocation());
		System.out.println("test Location.");
	}
	
	
	public void testSetAndGetImageUrl(){
		String testImageUrl = "aImageUrl";
		assertNull(grouping.getImageUrl());
		grouping.setImageUrl(testImageUrl);
		assertEquals(testImageUrl, grouping.getImageUrl());
		System.out.println("test ImageUrl.");
	}
	
	public void testSetAndGetCategory(){
		String testCategory = "aCategory";
		assertNull(grouping.getCategory());
		grouping.setCategory(testCategory);
		assertEquals(testCategory, grouping.getCategory());
		System.out.println("test Category.");
	}
	
	public void testSetAndGetDescription(){
		String testDescription = "aDescription";
		assertNull(grouping.getDescription());
		grouping.setDescription(testDescription);
		assertEquals(testDescription, grouping.getDescription());
		System.out.println("test Description.");
	}

	public void testSetAndGetFounder(){
		User testFounder = new User();
		assertNull(grouping.getFounder());
		grouping.setFounder(testFounder);
		assertEquals(testFounder, grouping.getFounder());
		System.out.println("test Founder.");
	}
	
	public void testSetAndGetMeetings(){
		List<Meeting> testMeetings = new ArrayList<Meeting>();
		assertNotNull(grouping.getMeetings());
		grouping.setMeetings(testMeetings);
		assertEquals(testMeetings, grouping.getMeetings());
		System.out.println("test Meetings.");
	}
	
	protected void tearDown() throws Exception{
		System.out.println("test tear down!");
	}
	

}