package au.usyd.elec5619.domain;

import junit.framework.TestCase;

public class UserRoleTest extends TestCase{
	
	private UserRole userRole;
	
	protected void setUp() throws Exception{
		userRole = new UserRole();
		System.out.println("test sets up!");
	}
	
	public void testSetAndGetUserRoleId(){
		Integer testUserRoleId = 123;
		assertNull(userRole.getUserRoleId());
		userRole.setUserRoleId(testUserRoleId);
		assertEquals(testUserRoleId, userRole.getUserRoleId());
		System.out.println("test UserRoleId.");
	}
	
	public void testSetAndGetUser(){
		User testUser = new User();
		assertNull(userRole.getUser());
		userRole.setUser(testUser);
		assertEquals(testUser, userRole.getUser());
		System.out.println("test User.");
	}
	
	public void testSetAndGetRole(){
		String testRole = "aRole";
		assertNull(userRole.getRole());
		userRole.setRole(testRole);
		assertEquals(testRole, userRole.getRole());
		System.out.println("test Role.");
	}
	
	protected void tearDown() throws Exception{
		System.out.println("test tear down!");
	}
}