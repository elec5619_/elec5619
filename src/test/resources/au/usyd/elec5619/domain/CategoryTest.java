package au.usyd.elec5619.domain;

import junit.framework.TestCase;

public class CategoryTest extends TestCase{
	
	private Category category;
	
	protected void setUp() throws Exception{
		category = new Category();
		System.out.println("test sets up!");
	}
	
	public void testSetAndGetId(){
		long testId = (long) 123;
		assertNotNull(category.getId());
		category.setId(testId);
		assertEquals(testId, category.getId());
		System.out.println("test Id.");
	}
	
	public void testSetAndGetCategoryName(){
		String testCategoryName = "sports";
		assertNull(category.getCategoryName());
		category.setCategoryName(testCategoryName);
		assertEquals(testCategoryName, category.getCategoryName());
		System.out.println("test CategoryName.");
	}
	
	protected void tearDown() throws Exception{
		System.out.println("test tear down!");
	}
}