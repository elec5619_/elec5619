package au.usyd.elec5619.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class MeetingTest extends TestCase{
	
	private Meeting meeting;
	
	protected void setUp() throws Exception{
		meeting = new Meeting();
		System.out.println("test sets up!");
	}
	
	public void testSetAndGetId(){
		long testId = (long) 123;
		assertNotNull(meeting.getId());
		meeting.setId(testId);
		assertEquals(testId, meeting.getId());
		System.out.println("test Id.");
	}
	
	public void testSetAndGetTitle(){
		String testTitle = "aTitle";
		assertNull(meeting.getTitle());
		meeting.setTitle(testTitle);
		assertEquals(testTitle, meeting.getTitle());
		System.out.println("test Title.");
	}
	
	public void testSetAndGetDescription(){
		String testDescription = "aDescription";
		assertNull(meeting.getDescription());
		meeting.setDescription(testDescription);
		assertEquals(testDescription, meeting.getDescription());
		System.out.println("test Description.");
	}
	
	public void testSetAndGetDate(){
		Date testDate = new Date();
		assertNull(meeting.getDate());
		meeting.setDate(testDate);
		assertEquals(testDate, meeting.getDate());
		System.out.println("test Date.");
	}
	
	public void testSetAndGetGroup(){
		Grouping testGroup = new Grouping();
		assertNull(meeting.getGroup());
		meeting.setGroup(testGroup);
		assertEquals(testGroup, meeting.getGroup());
		System.out.println("test Group.");
	}
	
	public void testSetAndGetOrganizer(){
		User testOrganizer = new User();
		assertNull(meeting.getOrganizer());
		meeting.setOrganizer(testOrganizer);
		assertEquals(testOrganizer, meeting.getOrganizer());
		System.out.println("test Organizer.");
	}
	
	public void testSetAndGetMembers(){
		Set<User> testMembers = new HashSet<User>();
		assertNotNull(meeting.getMembers());
		meeting.setMembers(testMembers);
		assertEquals(testMembers, meeting.getMembers());
		System.out.println("test Members.");
	}
	
	protected void tearDown() throws Exception{
		System.out.println("test tear down!");
	}
}