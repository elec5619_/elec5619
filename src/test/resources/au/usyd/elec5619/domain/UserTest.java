package au.usyd.elec5619.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class UserTest extends TestCase{
	
	private User user;
	
	protected void setUp() throws Exception{
		user = new User();
		System.out.println("test sets up!");
	}
	
	public void testSetAndGeId(){
		long testId = (long) 123;
		assertNotNull(user.getId());
		user.setId(testId);
		assertEquals(testId, user.getId());
		System.out.println("test Id.");
	}
	
	public void testSetAndGetUsername(){
		String testUsername = "Chunchun Dan";
		assertNull(user.getUsername());
		user.setUsername(testUsername);
		assertEquals(testUsername, user.getUsername());
		System.out.println("test Username.");
	}
	
	public void testSetAndGetPassword(){
		String testPassword = "1234567890";
		assertNull(user.getPassword());
		user.setPassword(testPassword);
		assertEquals(testPassword, user.getPassword());
		System.out.println("test Password.");
	}
	
	public void testSetAndGetGender(){
		String testGender = "female";
		assertNull(user.getGender());
		user.setGender(testGender);
		assertEquals(testGender, user.getGender());
		System.out.println("test Gender.");
	}
	
	public void testSetAndGetPhone(){
		String testPhone = "0401234567";
		assertNull(user.getPhone());
		user.setPhone(testPhone);
		assertEquals(testPhone, user.getPhone());
		System.out.println("test Phone.");
	}
	
	public void testSetAndGetBirthday(){
		Date testBirthday = new Date();
		assertNull(user.getBirthday());
		user.setBirthday(testBirthday);
		assertEquals(testBirthday, user.getBirthday());
		System.out.println("test Birthday.");
	}
	
	public void testSetAndGetEmail(){
		String testEmail = "cdan199221@gmail.com";
		assertNull(user.getEmail());
		user.setEmail(testEmail);
		assertEquals(testEmail, user.getEmail());
		System.out.println("test Email.");
	}
	
	public void testSetAndGetHometown(){
		String testHometown = "China";
		assertNull(user.getHometown());
		user.setHometown(testHometown);
		assertEquals(testHometown, user.getHometown());
		System.out.println("test Hometown.");
	}
	
	public void testSetAndGetLocation(){
		String testLocation = "aLocation";
		assertNull(user.getLocation());
		user.setLocation(testLocation);
		assertEquals(testLocation, user.getLocation());
		System.out.println("test Location.");
	}
	
	public void testSetAndGetPhoto(){
		String testPhoto = "aPhoto";
		assertNull(user.getPhoto());
		user.setPhoto(testPhoto);
		assertEquals(testPhoto, user.getPhoto());
		System.out.println("test Photo.");
	}
	
	public void testSetAndGetAddress(){
		String testAddress = "aAddress";
		assertNull(user.getAddress());
		user.setAddress(testAddress);
		assertEquals(testAddress, user.getAddress());
		System.out.println("test Address.");
	}
	
	public void testSetAndGetBio(){
		String testBio = "aBio";
		assertNull(user.getBio());
		user.setBio(testBio);
		assertEquals(testBio, user.getBio());
		System.out.println("test Bio.");
	}
	
	public void testSetAndGetLast_Login(){
		Date testLast_Login = new Date();
		assertNull(user.getLast_Login());
		user.setLast_Login(testLast_Login);
		assertEquals(testLast_Login, user.getLast_Login());
		System.out.println("test Last_Login.");
	}
	
	public void testSetAndGetIs_Superuser(){
		boolean testIs_Superuser = true;
		assertNull(user.getIs_Superuser());
		user.setIs_Superuser(testIs_Superuser);
		assertTrue(user.getIs_Superuser());
		System.out.println("test Bio.");
	}
	
	public void testSetAndGetDate_Joined(){
		Date testDate_Joined = new Date();
		assertNull(user.getDate_Joined());
		user.setDate_Joined(testDate_Joined);
		assertEquals(testDate_Joined, user.getDate_Joined());
		System.out.println("test Date_Joined.");
	}
	
	public void testSetAndGetUserRole(){
		Set<UserRole> testUserRole = new HashSet<UserRole>();
		assertNotNull(user.getUserRole());
		user.setUserRole(testUserRole);
		assertEquals(testUserRole, user.getUserRole());
		System.out.println("test UserRole.");
	}
	
	public void testSetAndGetEnabled(){
		boolean testEnabled = true;
		assertFalse(user.isEnabled());
		user.setEnabled(testEnabled);
		assertTrue(user.isEnabled());
		System.out.println("test Enabled.");
	}
	
	public void testSetAndGetFoundGroupList(){
		Set<Grouping> testFoundGroupList = new HashSet<Grouping>();
		assertNotNull(user.getFoundGroupList());
		user.setFoundGroupList(testFoundGroupList);
		assertEquals(testFoundGroupList, user.getFoundGroupList());
		System.out.println("test FoundGroupList.");
	}
	
	public void testSetAndGetJoinGroupList(){
		Set<Grouping> testJoinGroupList = new HashSet<Grouping>();
		assertNotNull(user.getJoinGroupList());
		user.setJoinGroupList(testJoinGroupList);
		assertEquals(testJoinGroupList, user.getJoinGroupList());
		System.out.println("test JoinGroupList.");
	}
	
	public void testSetAndGetMeetingList(){
		Set<Meeting> testMeetingList = new HashSet<Meeting>();
		assertNotNull(user.getMeetingList());
		user.setMeetingList(testMeetingList);
		assertEquals(testMeetingList, user.getMeetingList());
		System.out.println("test Meetings.");
	}
	
	protected void tearDown() throws Exception{
		System.out.println("test tear down!");
	}
}